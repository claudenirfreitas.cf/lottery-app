# Lottery API

A simple lottery system.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

| Technologies   |  Version   | Required   | 
| -------------  |  ----------| ---------- | 
| Java           |  11        | True       | 
| Apache Maven   |  3.6.3     | True       |
| Docker         |  19.03.12  | True       | 
| Docker Compose |  1.26.2    | True       | 
| GNU Make       |  4.2.1     | Optional   | 

For more details, check it out [pom.xml](https://bitbucket.org/ClaudenirFreitas/lottery/src/master/pom.xml).

### Installing

A step by step series of examples that tell you how to get a development env running

### Generate application image
```
sh generate-image.sh # -DskipTests to ignore tests
# or
make generate-image
```

### Infra (MySQL, Prometheus, Grafana)
```
docker-compose -f docker/docker-compose.yml up -d lottery-db prometheus grafana
# or
make start-infra
```

### Application (On container)
```
docker-compose -f docker/docker-compose.yml up -d lottery-app
# or
make start-app
```

### Application (Dev mode)
```
mvn clean spring-boot:run
```

After that:

| Applications   |  Address                                                                           | 
| -------------  |  --------------------------------------------------------------------------------- | 
| lottery-app    |  http://localhost:8080                                                             | 
| API Doc        |  http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config |
| Prometheus     |  http://localhost:9090/                                                            |
| Grafana        |  http://localhost:3000/ *(username: admin /  password: admin)*                     |  

## Running the tests

To run unit tests:

```
mvn clean -Dgroups="unit-tests" test
```

To run integration tests:

```
mvn clean -Dgroups="integration-tests" test
```

To run all tests:

```
mvn clean test
```

## Deployment

This project uses Bitbucket pipelines to push the application image to Docker Hub after any commit.
It's possible to improve this pipeline using [Gitflow](https://nvie.com/posts/a-successful-git-branching-model/).

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [Docker](https://www.docker.com/) - A standardized unit of software
* [Make](https://www.docker.com/) - A tool which controls the generation of executables and other non-source files of a program from the program's source files

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://hub.docker.com/r/claudenirfreitas/lottery-app). 

## Author

* **Claudenir Freitas** - [GitHub](https://github.com/claudenirfreitas)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Poppulo for this opportunity
