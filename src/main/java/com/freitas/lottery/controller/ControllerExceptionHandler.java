package com.freitas.lottery.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.freitas.lottery.dto.ErrorDTO;
import com.freitas.lottery.exception.InvalidStatusException;
import com.freitas.lottery.exception.TicketNotFoundException;
import com.freitas.lottery.exception.TicketStatusException;

@RestControllerAdvice
public class ControllerExceptionHandler {

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(TicketNotFoundException.class)
	protected ResponseEntity<ErrorDTO> handleEntityNotFound(TicketNotFoundException ex) {
		return new ResponseEntity<>(new ErrorDTO(HttpStatus.NOT_FOUND, ex), HttpStatus.NOT_FOUND);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(TicketStatusException.class)
	protected ResponseEntity<ErrorDTO> handleEntityNotPossibleChangeStatus(TicketStatusException ex) {
		return new ResponseEntity<>(new ErrorDTO(HttpStatus.BAD_REQUEST, ex), HttpStatus.BAD_REQUEST);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(InvalidStatusException.class)
	protected ResponseEntity<ErrorDTO> handleEntityInvalidStatus(InvalidStatusException ex) {
		return new ResponseEntity<>(new ErrorDTO(HttpStatus.BAD_REQUEST, ex), HttpStatus.BAD_REQUEST);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(HttpMessageNotReadableException.class)
	protected ResponseEntity<ErrorDTO> handleInvalidBody(HttpMessageNotReadableException ex) {
		return new ResponseEntity<>(new ErrorDTO(HttpStatus.BAD_REQUEST, "Invalid format!"), HttpStatus.BAD_REQUEST);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorDTO> processValidationError(MethodArgumentNotValidException ex) {
		BindingResult bindingResult = ex.getBindingResult();
		return new ResponseEntity<>(
				new ErrorDTO(HttpStatus.BAD_REQUEST, bindingResult.getFieldError().getDefaultMessage()),
				HttpStatus.BAD_REQUEST);
	}

}