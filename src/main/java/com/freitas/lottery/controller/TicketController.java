package com.freitas.lottery.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.freitas.lottery.dto.TicketDTO;
import com.freitas.lottery.entity.Ticket;
import com.freitas.lottery.entity.TicketLine;
import com.freitas.lottery.entity.TicketStatus;
import com.freitas.lottery.service.ITicketService;
import com.freitas.lottery.util.ParseUtil;

@RestController
@RequestMapping("/api/v1/tickets")
public class TicketController {
	
	private	final ParseUtil parser;
	private final ITicketService service;

	public TicketController(final ParseUtil parser,
                            final ITicketService service) {
		this.parser = parser;
		this.service = service;
	}
	
	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public List<TicketDTO> findAll(@RequestParam(defaultValue = "10", required = false) int size,
			                       @RequestParam(defaultValue = "0", required = false) int page) {
		return service.findAll(PageRequest.of(page, size))
				      .getContent()
				      .stream()
				      .map(parser::toTicketDTO)
				      .collect(Collectors.toList());
	}
	
	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	@ResponseStatus(code = HttpStatus.OK)
	public TicketDTO findById(@PathVariable final Long id) {
		Ticket ticket = service.find(id);
		return parser.toTicketDTO(ticket);
	}

	@GetMapping(value = "/{id}/status", produces = MediaType.TEXT_PLAIN_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public String getStatusById(@PathVariable final Long id) {
		return service.findStatusById(id).name();
	}

	@PatchMapping(value = "/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public void updateStatusById(@PathVariable final Long id,
			                     @Valid @RequestBody TicketDTO ticket) {

		List<TicketLine> lines = ticket.getLines()
				                       .stream()
				                       .map(parser::toTicketLineEntity)
				                       .collect(Collectors.toList());
		
		service.addLinesOnTicketBydId(id, lines);
	}

	@PatchMapping(value = "/{id}/status", consumes = MediaType.TEXT_PLAIN_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public void updateStatusById(@PathVariable final Long id,
			                     @RequestBody String status) {
		service.updateStatusById(id, TicketStatus.get(status));
	}
	
	@PostMapping
	public ResponseEntity<TicketDTO> create() {
		Ticket ticket = service.create();

		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				                                  .path("/{id}")
				                                  .buildAndExpand(ticket.getId())
				                                  .toUri();
		return ResponseEntity.created(location)
				             .body(parser.toTicketDTO(ticket));
	}

	@PutMapping
	public ResponseEntity<TicketDTO> update() {
		Ticket ticket = service.create();

		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				                                  .path("/{id}")
				                                  .buildAndExpand(ticket.getId())
				                                  .toUri();
		return ResponseEntity.created(location)
				             .body(parser.toTicketDTO(ticket));
	}

}
