package com.freitas.lottery.service;

import java.util.Objects;

import com.freitas.lottery.entity.TicketLine;

public abstract class Rule {

	private final int DEFAULT_VALUE = 0;

	private final Rule nextRule;

	public abstract boolean check(final TicketLine line);

	public abstract int value();

	public Rule() {
		this.nextRule = null;
	}

	public Rule(final Rule nextRule) {
		this.nextRule = nextRule;
	}

	public final int process(final TicketLine line) {

		boolean valid = check(line);
		if (valid) {
			return value();
		}

		if (Objects.nonNull(nextRule)) {
			return nextRule.process(line);
		}

		return DEFAULT_VALUE;
	}
	
}
