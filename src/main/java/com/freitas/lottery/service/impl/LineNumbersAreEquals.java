package com.freitas.lottery.service.impl;

import java.util.Objects;

import com.freitas.lottery.entity.TicketLine;
import com.freitas.lottery.service.Rule;

public final class LineNumbersAreEquals extends Rule {
	
	public LineNumbersAreEquals() {
		super();
	}

	public LineNumbersAreEquals(final Rule nextRule) {
		super(nextRule);
	}

	@Override
	public boolean check(final TicketLine line) {
		return Objects.equals(line.getFirstNumber(), line.getSecondNumber()) &&
			   Objects.equals(line.getSecondNumber(), line.getThirdNumber());
	}

	@Override
	public int value() {
		return 5;
	}

}
