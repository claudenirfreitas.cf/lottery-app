package com.freitas.lottery.exception;

public class TicketStatusException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public TicketStatusException() {
		super("Ticket with status CHECKED!");
	}

}
