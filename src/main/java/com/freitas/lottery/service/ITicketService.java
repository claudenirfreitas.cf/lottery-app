package com.freitas.lottery.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.freitas.lottery.entity.Ticket;
import com.freitas.lottery.entity.TicketLine;
import com.freitas.lottery.entity.TicketStatus;
import com.freitas.lottery.exception.TicketNotFoundException;
import com.freitas.lottery.exception.TicketStatusException;

public interface ITicketService {

	Ticket create();

	Ticket find(final Long id) throws TicketNotFoundException;

	TicketStatus findStatusById(final Long id) throws TicketNotFoundException;

	void addLinesOnTicketBydId(final Long id, final List<TicketLine> lines);

	void updateStatusById(final Long id, final TicketStatus status) throws TicketNotFoundException, TicketStatusException;

	Page<Ticket> findAll(final Pageable pageable);

}