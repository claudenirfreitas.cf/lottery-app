package com.freitas.lottery.controller;

import javax.annotation.PostConstruct;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;

import com.freitas.lottery.entity.Ticket;
import com.freitas.lottery.entity.TicketStatus;
import com.freitas.lottery.repository.TicketRepository;
import com.freitas.lottery.service.ITicketService;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@Tag("integration-tests")
@ContextConfiguration(initializers = DatabaseInitializer.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TicketControllerGetTest {

	@LocalServerPort 
	private int port;
	
	@Autowired
	private ITicketService service;
	
	@Autowired
	private TicketRepository repository;
	
	@PostConstruct
    public void init() {
        RestAssured.port = port;
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }
	
	@BeforeEach
	void setup() {
		repository.deleteAll();
	}

	@Test
	void shouldReturnEmptyTickets() {
		RestAssured.given()
		           .when()
		               .get("/api/v1/tickets")
		           .then()
		               .statusCode(HttpStatus.OK.value())
		               .body("$", Matchers.emptyIterable());
	}

	@Test
	void shouldReturnOneTicket() {
		
		Ticket ticket = service.create();

		RestAssured.given()
		           .when()
		               .get("/api/v1/tickets")
		           .then()
		               .statusCode(HttpStatus.OK.value())
		               .contentType(ContentType.JSON)
		               .body("$", Matchers.hasSize(1))
		               .body("[0].id", Matchers.equalTo(ticket.getId().intValue()))
		               .body("[0].lines.size()", Matchers.equalTo(ticket.getLines().size()));
	}

	@Test
	void shouldReturnOneTicketUsingPagination() {
		
		Ticket ticket01 = service.create();
		Ticket ticket02 = service.create();
		service.create();
		service.create();

		RestAssured.given()
		               .queryParam("size", 2)
		           .when()
		               .get("/api/v1/tickets")
		           .then()
		               .statusCode(HttpStatus.OK.value())
		               .contentType(ContentType.JSON)
		               .body("$", Matchers.hasSize(2))
		               .body("id", Matchers.hasItems(ticket01.getId().intValue(),
		            		                         ticket02.getId().intValue()));
	}

	@Test
	void shouldReturnATicket() {
		
		Ticket ticket = service.create();

		RestAssured.given()
				       .pathParam("id", ticket.getId())
		           .when()
		               .get("/api/v1/tickets/{id}")
		           .then()
		               .statusCode(HttpStatus.OK.value())
		               .contentType(ContentType.JSON)
		               .body("id", Matchers.equalTo(ticket.getId().intValue()))
		               .body("lines.size()", Matchers.equalTo(ticket.getLines().size()));
	}

	@Test
	void shouldReturnNotFound() {
		
		RestAssured.given()
		           .when()
		               .get("/api/v1/tickets/-1")
		           .then()
		               .statusCode(HttpStatus.NOT_FOUND.value())
		               .contentType(ContentType.JSON);
	}
	
	@Test
	void shouldReturnStatusOfTicket() {
		
		Ticket ticket = service.create();

		RestAssured.given()
			           .pathParam("id", ticket.getId())
		           .when()
		               .get("/api/v1/tickets/{id}/status")
		           .then()
		               .statusCode(HttpStatus.OK.value())
		               .contentType(ContentType.TEXT)
		               .body(Matchers.equalTo(TicketStatus.NOT_CHECKED.name()));
	}

}